import './App.css';
import React from 'react';
import Places from './components/Places';
import Weekly from './components/Weekly';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useParams,
  useLocation
} from 'react-router-dom';

function App() {
  return (

    <Router>
    <div className="container">
        <nav>
          <ul className="Nav">
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/places">Map</Link>
            </li>
            <li>
              <Link to="/weekly">Weekly Forecast</Link>
            </li>
            <li>
              <Link to="/comments">Comments</Link>
            </li>
          </ul>
        </nav>

        {/* */}

      <div className="App">
        <section>
          <Switch>
            <Route path="/weekly/:id">
              <Weekly />
            </Route>
            <Route path="/comments">
              <Comments />
            </Route>
            <Route path="/places">
              <Places />
            </Route>
            <Route path="/">
              <h1 style={{fontSize: ' 45px', color: '#0000FF'}}>BBC</h1>
              <h3>This is our main page. BBC</h3>
              <br></br>
              <p style={{fontSize: ' 25px', color: '#1F8873'}}>Welcome to check <Link to={'/places'}>map</Link> or weekly forecast <Link to={'/weekly'}>now</Link></p>
            </Route>
            <Route path="*">
              <NoMatch />
            </Route>
          </Switch>
          <Route path="/weekly">
              <Redirect to="/weekly/2021-04-20" />
          </Route>
        </section>      
      </div> 
    </div>
    </Router>
  );
}

function NoMatch() {
  let location = useLocation();

  return (
    <div>
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  );
}

function Comments() {
  return <div>
         
            <h2 style={{fontSize: ' 30px', color: '#071D70'}}>Comments of our users and attenders<br></br></h2>
            <div style={{fontSize: ' 20px', backgroundColor: '#7FFFD4', textAlign: 'left', padding: ' 20px', marginTop: '20px'}}>
                <p>
                Lorem ipsum dolor sit amet consectetur, adipiscing elit ligula pharetra, 
                potenti sagittis eget inceptos. Pretium ante rhoncus sapien nunc nostra dictumst ac magna quis sollicitudin risus,
                </p>
            </div>
            <div style={{fontSize: ' 20px', backgroundColor: '#7FFFD4', textAlign: 'left', padding: ' 20px', marginTop: '20px'}}>
                <p>
                Nostra enim tellus dictumst porta justo habitasse convallis pellentesque ullamcorper, 
                parturient posuere gravida felis mi varius rhoncus viverra, imperdiet mus id porttitor tortor vulputate facilisis iaculis.
                </p>
            </div>
            <div style={{fontSize: ' 20px', backgroundColor: '#7FFFD4', textAlign: 'left', padding: ' 20px', marginTop: '20px'}}>
                <p>
                Lorem ipsum dolor sit amet consectetur, adipiscing elit ligula pharetra, 
                potenti sagittis eget inceptos. 
                </p>
            </div>
        </div>
}

export default App;
