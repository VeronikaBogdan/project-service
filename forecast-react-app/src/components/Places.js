import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import { Link } from "react-router-dom";
import Moment from 'react-moment';
import moment from 'moment';


const api = new Api.DefaultApi()

class Places extends React.Component {

    constructor(props) {
        super(props);
        const dateParam = this.props.match?.params.id || moment().format('YYYY-MM-DD');
        const parsedDate = moment(dateParam, "YYYY-MM-DD")

        const nearestWeekend = parsedDate.startOf('week').isoWeekday(0);
        const endDate = moment(nearestWeekend).add(6, 'day');

        console.log("Enddate", nearestWeekend.toString(), endDate.toString())

        const startWeek = nearestWeekend.format("YYYY-MM-DD")
        const endWeek = endDate.format("YYYY-MM-DD")



        // TODO reuse data to service from https://citydog.by/post/play-musykanty/
        this.state = {
            places: [],
            start: startWeek,
            end: endWeek

        };

        console.log(this.state.start, this.state.end)
        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


    async handleReload(event) {
        const response = await api.places({ date: this.state.startWeek });
        this.setState({ places: response });
    }


    render() {
        return <div>
            {/*<button onClick={this.handleReload}>Reload</button>*/}
            <h1>Weekly Map Month</h1>
            <h3>Upcoming places from  <Moment format="YYYY/MM/DD">{this.state.start}</Moment> to <Moment format="YYYY/MM/DD">{this.state.end}</Moment> </h3>
            <div>
                {this.state.places.map(
                    (place) =>
                        <div key={place.country} style={{ float:'left', paddingBottom: '10px'}}>
                            <div style={{fontSize: ' 20px', color: '#191970'}}>In {place.month}</div>
                            <div style={{fontSize: ' 15px', color: 'RoyalBlue'}}><b>City</b> {place.city}</div>
                            <div style={{fontSize: ' 15px', color: '#008080'}}>Температура меняется от - <b>{place.tempmin} до {place.tempmax}</b> </div>
                            <div>Атмосферное давление: <b>{place.pressure} рт.ст. </b></div>
                            <div>Вероятность осадков:<b>{place.rainfall} %</b> </div>
                            <div><img src = {place.image} width="300" height="200"></img></div>
                         </div>)}
            </div>
        </div>
    }
}


export default withRouter(Places);