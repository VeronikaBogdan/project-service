import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import moment from 'moment'
import Moment from 'react-moment';


const api = new Api.DefaultApi()

class Weekly extends React.Component {

    constructor(props) {
        super(props);
        const id =  this.props.match?.params.id || moment().format('LLLL');
        console.log(id);

        

        this.state = { 
            weekly: [],
            date: id 
        };

        this.handleReload = this.handleReload.bind(this);
        //this.handleReload();
    }


    async handleReload(event) {
        const response = await api.weekly({ date: ''/*this.state.targetDate*/ });
        this.setState({ weekly: response});
    }


    render() {
        return <div>
            <button onClick={this.handleReload}>WEATHER</button> 
            <h2>Weather scheduler in: </h2>
            <ul style={{fontSize: ' 25px', color: '#1F8873', listStyle: 'none'}}>
               {this.state.weekly.map(
                   (weekly) => 
                        <li style={{fontSize: ' 30px', color: 'DarkBlue', fontFamily:'"Gill Sans", sans-serif'}} > {weekly.city} </li>)}
            </ul>
            <h3>recent update on <Moment format="LLLL">{this.state.date}</Moment> </h3>
            <ul style={{fontSize: ' 25px', color: '#1F8873', listStyle: 'none'}}>
               {this.state.weekly.map(
                   (weekly) => 
                     <li style={{fontSize: ' 25px', color: 'RoyalBlue', fontFamily:'serif', padding:'40px', paddingBottom:'25px', float:'left'}} > 
                        <div style={{color: '#191970'}}>Info about the weathure in </div>
                        <div><strong>{weekly.city}</strong> → DAY: <strong>{weekly.weekday}</strong></div>
                        <div>(humidity at the moment:  {weekly.hum}) </div>
                        <div>degrees: -{weekly.tempmin}°, {weekly.tempmax}°</div>
                    </li>)}
             </ul>
        </div>
    }

}

export default withRouter(Weekly);